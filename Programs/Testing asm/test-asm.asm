section .text

global find_gcd_win32
global find_gcd

find_gcd_internal: ;rdi是第一個參數，rsi是第二個參數，返回值放在rax
    cmp rdi, rsi
    ja sec1
    xchg rdi, rsi

sec1:
    push rbx
    push rcx
    push rdx
    mov rax, rdi
    mov rbx, rsi
    mov rcx, rdi
    xor rdx, rdx

loop1:
    cmp rbx, 0
    jne sec2
    pop rdx
    pop rcx
    pop rbx
    jmp find_gcd_internal_exit ;此時rax即為gcd

sec2:
    div rbx
    cmp rdx, 0
    je sec3
    mov rax, rbx
    mov rbx, rdx
    xor rdx, rdx
    loop loop1

sec3:
    mov rax, rbx
    pop rdx
    pop rcx
    pop rbx
find_gcd_internal_exit:
    ret

find_gcd_win32:
    push rdi
    push rsi
    mov rdi, rcx
    mov rsi, rdx
    call find_gcd_internal
    pop rsi
    pop rdi
    ret

find_gcd:
    call find_gcd_internal
    ret