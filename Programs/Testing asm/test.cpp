#include <iostream>
#include <cstring>
#include <regex>
#include <string>

using namespace std;

extern "C"
{
#ifdef _WIN32
    int find_gcd_win32(int, int);
#else
    int find_gcd(int, int);
#endif
}

void help(char**);

int main(int argc, char *argv[])
{
    if (argc > 3 || argc == 2)
    {
        help(argv);
        return 1;
    }

    if (argc == 1 ||
        strcmp(argv[1], "-h") == 0 ||
        strcmp(argv[1], "--help") == 0)
    {
        help(argv);
        return 0;
    }

    regex num("[0-9]*");
    if (regex_match(string(argv[1]), num) == false ||
        regex_match(string(argv[2]), num) == false ||
        atoi(argv[1]) == 0 ||
        atoi(argv[2]) == 0)
    {
        cerr << "invalid inputs!" << endl;
        return 1;
    }

    int res = 0;
#ifdef _WIN32
    res = find_gcd_win32(atoi(argv[1]), atoi(argv[2]));
#else
    res = find_gcd(atoi(argv[1]), atoi(argv[2]));
#endif
    cout << res << endl;
    return 0;
}

void help(char **argv)
{
    cout << argv[0] << " usage:" << endl;
    cout << argv[0] << " input1 input2" << endl;
    cout << "input1 and input2 must be number." << endl;
}