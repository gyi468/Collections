#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <regex>
#include <string>
#include <exception>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

using namespace std;
using namespace boost::property_tree;

void help() {
	cout << "Usage:" << endl;
	cout << "--max: Set maximum value. Default is 100." << endl;
	cout << "--min: Set minimum value. Default is 0." << endl;
	cout << "--start: Just start this program with default setting." << endl;
	cout << "--help: Print this help." << endl;
	cout << "You can also put \"setting.json\" and this binary in same folder," << endl;
	cout << "and this program will handle \"setting.json\"." << endl;
	cout << "For detail, please read \"README.md\"." << endl;
}

int main(int argc, char *argv[]) {

	int max(100);
	int min(0);
	regex dig("^[0-9]{1,45}$");
	string arg;

	if (argc == 2) {
		arg = argv[1];
		if (arg == "--help") {
			help();
			return 0;
		}
		else if (arg == "--start") {
			goto process;
		}
		else {
			cout << "Invalid argument! Please use \"--help\" for help." << endl;
			return 1;
		}
	}
	else if ((argc > 5) || (argc % 2 != 1)) {
		cout << "Invalid argument(s)! Please use \"--help\" for help." << endl;
		return 1;
	}
	else if (argc == 1) {
		try
		{
			ptree tree;
			read_json("setting.json", tree);

			max = tree.get<int>("max");
			min = tree.get<int>("min");
		}
		catch (exception &e)
		{
			cout << e.what() << endl;
			cout << "Please use \"--help\" for help." << endl;
			return 1;
		}
	}

	if ((argc > 3) && (argv[1] == argv[3])) {
		cout << "Invalid argument(s)! Please use \"--help\" for help." << endl;
		return 1;
	}

	for (int i = 1; i <= (argc - 1); i += 2) {
		if (!(regex_match(argv[i + 1], dig))) {
			cout << "Invalid argument(s)! Please use \"--help\" for help." << endl;
			return 1;
		}

		arg = argv[i];

		if (arg == "--max") {
			max = atoi(argv[i + 1]);
		}
		else if (arg == "--min") {
			min = atoi(argv[i + 1]);
		}
		else {
			cout << "Invalid argument(s)! Please use \"--help\" for help." << endl;
			return 1;
		}
	}

	if (max == min) {
		cout << "Your \"max\" and \"min\" are same!" << endl;
		return 1;
	}
	else if ((max < 0) || (min < 0)) {
		cout << "Both \"max\" and \"min\" CANNOT be negative!" << endl;
		return 1;
	}
	else if (min > max) {
		cout << "\"min\" is greater than \"max\"!" << endl;
		return 1;
	}

process:

	int k(1); //for loop
	srand(time(NULL));
	int target = rand() % (max - min + 1) + min;
	string x;
	int y;
	int process_max(max);
	int process_min(min);

	while (k > 0) {
		cout << "Please input the number between " + to_string(process_max) + " and " + to_string(process_min) + ": " << endl;
		cin >> x;

		try
		{
			y = stoi(x);
		}
		catch(exception &e)
		{
			k = 1;
		}

		if ((y > process_max) || (y < process_min)) {
			cout << "Input is invalid! Please try again." << endl;
			k = 1;
		}
		else if (y < target) {
			process_min = y;
			k = 1;
		}
		else if (y > target) {
			process_max = y;
			k = 1;
		}
		else {
			cout << "You lose!" << endl;

			string ans;
			int j(1);

			while (j > 0) {
				cout << "Try again? [Y/n]" << endl;
				cin >> ans;

				if ((ans == "N") || ans == "n") {
					j = 0;
				}
				else if ((ans == "Y") || (ans == "y")) {
					goto process;
				}
				else {
					cout << "Invaild input!" << endl;
					j = 1;
				}
			}
			k = 0;
		}
	}
	return 0;
}
