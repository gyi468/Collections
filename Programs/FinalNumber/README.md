# Final Number
## 簡介
簡單的終極密碼 
主要是要測試Boost及Qt

GUI版本中的resource來自[Silk icons](http://www.famfamfam.com/lab/icons/silk/)  
[Silk icons](http://www.famfamfam.com/lab/icons/silk/)使用[Creative Commons Attribution 2.5 License](https://creativecommons.org/licenses/by/2.5/)釋出

## Introduction
This is a simple game called Final Number. The reason why I created it is I want to test Boost and Qt.  

In GUI version, icons in "resource" folder are from [Silk icons](http://www.famfamfam.com/lab/icons/silk/), distributed under [Creative Commons Attribution 2.5 License](https://creativecommons.org/licenses/by/2.5/).

## Dependencies
- [Boost](http://www.boost.org/)
- [Qt](https://www.qt.io/) (GUI version only)
