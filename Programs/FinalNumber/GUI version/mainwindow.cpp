#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "regex"
#include "exception"
#include "string"

#include "QMessageBox"
#include "QFileDialog"
#include "QDir"

#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/json_parser.hpp"
#include "boost/filesystem.hpp"

using namespace std;
using namespace boost::property_tree;
namespace bf = boost::filesystem;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    save_path(""),
    number("^[0-9]{1,45}$")
{
    ui->setupUi(this);
    ui->actionSave_config->setEnabled(false);
    ui->actionSave_config_as->setEnabled(false);
    ui->input->setEnabled(false);
    ui->ok->setEnabled(false);
    ui->clean2->setEnabled(false);
	
    try
    {
        ptree tree;
        read_json("setting.json", tree);

        ui->max->setText(QString::number(tree.get<int>("max")));
        ui->min->setText(QString::number(tree.get<int>("min")));

        string temp_path = bf::current_path().string();

#ifdef _WIN32
        string::size_type i = 0;
        while((i = temp_path.find("\\", i)) != string::npos) {
            temp_path.replace(i, 1, "/");
            ++i;
        }
#endif
        setWindowTitle("FinalNumber_GUI - " + QString::fromStdString(temp_path + "/setting.json"));
    }
    catch (...)
    {
        ui->max->setText("100");
        ui->min->setText("0");
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionOpen_config_triggered()
{
    QString path = QFileDialog::getOpenFileName(this, "Open config", QDir::currentPath(), "config file(*.json)");
    if (path != "") {
        save_path = path;
        ptree tree;
        read_json(path.toUtf8().constData(), tree);

        try
        {
            ui->max->setText(QString::number(tree.get<int>("max")));
            ui->min->setText(QString::number(tree.get<int>("min")));
            setWindowTitle("FinalNumber_GUI - " + path);
        }
        catch (exception &e) {
            QMessageBox::critical(this, "Error", QString::fromStdString(e.what()));
        }
    }
}

void MainWindow::on_actionSave_config_triggered()
{
    if (save_path == "")
        save_path = QFileDialog::getSaveFileName(this, "Save config", QDir::currentPath(), "config file(*.json)");

    if (save_path != "") {
        ptree tree;

        tree.put("max", ui->max->text().toInt());
        tree.put("min", ui->min->text().toInt());

        write_json(save_path.toUtf8().constData(), tree);
        setWindowTitle("FinalNumber_GUI - " + save_path);
        ui->actionSave_config->setEnabled(false);
        QMessageBox::information(this, "Save File", "Success!");
    }
}

void MainWindow::on_actionSave_config_as_triggered()
{
    save_path = QFileDialog::getSaveFileName(this, "Save config as", QDir::currentPath(), "config file(*.json)");

    if (save_path != "") {
        ptree tree;

        tree.put("max", ui->max->text().toInt());
        tree.put("min", ui->min->text().toInt());

        write_json(save_path.toUtf8().constData(), tree);
        ui->actionSave_config->setEnabled(false);
        setWindowTitle("FinalNumber_GUI - " + save_path);
        QMessageBox::information(this, "Save File", "Success!");
    }
}

void MainWindow::on_clean_clicked()
{
    ui->max->setText("");
    ui->min->setText("");
    setWindowTitle("FinalNumber_GUI");
}

void MainWindow::on_start_clicked()
{
    max = ui->max->text().toInt();
    min = ui->min->text().toInt();

    if (min > max) {
        QMessageBox::warning(this, "Error", "\"min\" is greater than \"max\"!");
        return;
    }
    else if ((max < 0) || (min < 0)) {
        QMessageBox::warning(this, "Error", "Both \"max\" and \"min\" CANNOT be negative!");
        return;
    }
    else if (max == min) {
        QMessageBox::warning(this, "Error", "Your \"max\" and \"min\" are same!");
        return;
    }

    temp = ui->actionSave_config->isEnabled();

    ui->max->setEnabled(false);
    ui->min->setEnabled(false);
    ui->start->setEnabled(false);
    ui->clean->setEnabled(false);
    ui->actionOpen_config->setEnabled(false);
    ui->actionSave_config->setEnabled(false);
    ui->actionSave_config_as->setEnabled(false);

    ui->input->setEnabled(true);
    ui->clean2->setEnabled(true);

    srand(time(NULL));
    target = rand() % (max - min + 1) + min;

    QString txt = "Please input the number between " + QString::number(max) + " and " + QString::number(min) + ".";

    ui->info->setText(txt);
}

void MainWindow::on_max_textChanged(const QString &arg1)
{
    setWindowTitle("FinalNumber_GUI - " + save_path + "*");
    if
    (
    (regex_match(ui->max->text().toUtf8().constData(), number)) &&
    (regex_match(ui->min->text().toUtf8().constData(), number))
    ) {
        ui->actionSave_config->setEnabled(true);
        ui->actionSave_config_as->setEnabled(true);
        ui->start->setEnabled(true);
    }
    else {
        ui->actionSave_config->setEnabled(false);
        ui->actionSave_config_as->setEnabled(false);
        ui->start->setEnabled(false);
    }
}

void MainWindow::on_min_textChanged(const QString &arg1)
{
    setWindowTitle("FinalNumber_GUI - " + save_path + "*");
    if
    (
    (regex_match(ui->max->text().toUtf8().constData(), number)) &&
    (regex_match(ui->min->text().toUtf8().constData(), number))
    ) {
        ui->actionSave_config->setEnabled(true);
        ui->actionSave_config_as->setEnabled(true);
        ui->start->setEnabled(true);
    }
    else {
        ui->actionSave_config->setEnabled(false);
        ui->actionSave_config_as->setEnabled(false);
        ui->start->setEnabled(false);
    }
}

void MainWindow::on_clean2_clicked()
{
    ui->input->setText("");
}

void MainWindow::on_input_textChanged(const QString &arg1)
{
    if
    (
    (regex_match(ui->input->text().toUtf8().constData(), number))
    ) {
        ui->ok->setEnabled(true);
    }
    else {
        ui->ok->setEnabled(false);
    }
}

void MainWindow::on_ok_clicked()
{
    ui->ok->setEnabled(false);
    int in = ui->input->text().toInt();
    ui->input->setText("");

    QString txt;
    if ((in > max) || (in < min)) {
        QMessageBox::warning(this, "Error", "Input is invalid! Please try again.");
        ui->ok->setEnabled(true);
    }
    else if (in < target) {
        min = in;
        txt = "Please input the number between " + QString::number(max) + " and " + QString::number(min) + ".";
        ui->info->setText(txt);
        ui->ok->setEnabled(true);
    }
    else if (in > target) {
        max = in;
        txt = "Please input the number between " + QString::number(max) + " and " + QString::number(min) + ".";
        ui->info->setText(txt);
        ui->ok->setEnabled(true);
    }
    else {
        ui->info->setText("");

        QMessageBox::StandardButton ans;
        ans = QMessageBox::question(this, "Again?", "You lose! Try again?", QMessageBox::Yes|QMessageBox::No);

        if (ans == QMessageBox::Yes) {
            on_start_clicked();
        }
        else {
            ui->info->setEnabled(false);
            ui->input->setEnabled(false);
            ui->clean2->setEnabled(false);

            ui->max->setEnabled(true);
            ui->min->setEnabled(true);
            ui->start->setEnabled(true);
            ui->clean->setEnabled(true);
            ui->actionOpen_config->setEnabled(true);
            ui->actionSave_config_as->setEnabled(true);

            if (temp)
                ui->actionSave_config->setEnabled(true);
        }
    }
}
