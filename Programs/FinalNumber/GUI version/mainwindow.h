#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "regex"

using namespace std;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    
    ~MainWindow();

private slots:
    void on_actionOpen_config_triggered();

    void on_actionSave_config_triggered();

    void on_actionSave_config_as_triggered();

    void on_clean_clicked();

    void on_start_clicked();

    void on_max_textChanged(const QString &arg1);

    void on_min_textChanged(const QString &arg1);

    void on_clean2_clicked();

    void on_input_textChanged(const QString &arg1);

    void on_ok_clicked();

private:
    Ui::MainWindow *ui;
    
    QString save_path;
    
    int max;
    
    int min;
    
    int target;
    
    regex number;

    bool temp;
};

#endif // MAINWINDOW_H
