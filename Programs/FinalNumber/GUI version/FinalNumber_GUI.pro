QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FinalNumber_GUI
TEMPLATE = app

contains(QMAKE_COMPILER, clang) {
    QMAKE_CXXFLAGS += -std=c++11
}

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

RESOURCES += \
    resource.qrc

unix {
    INCLUDEPATH += -I/path/to/your/include
    LIBS += /path/to/your/boost/libs
}
win32 {
    INCLUDEPATH += X:\path\to\your\boost
    LIBS += X:\path\to\your\boost\libs
}
