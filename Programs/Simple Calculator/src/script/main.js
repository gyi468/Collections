var calculator_process = function()
{
    this.temp = 0; //screen value
    this.input1 = 0;
    this.input2 = 0;
    this.mode = 0; //mode 1~4 分別代表加減乘除, 0代表還沒輸入加減乘除
}

//class methods
calculator_process.prototype.exec = function()
{
    switch (this.mode)
    {
        case 1:
            this.input1 += this.input2;
            this.input2 = 0;
            break;
        
        case 2:
            this.input1 -= this.input2;
            this.input2 = 0;
            break;
        
        case 3:
            this.input1 *= this.input2;
            this.input2 = 0;
            break;
        
        case 4:
            this.input1 /= this.input2;
            this.input2 = 0;
            break;
        
        default:
            break;
    } //end switch

    return this.input1;
}; //end calculator_process.prototype.exec

calculator_process.prototype.main_process = function(input, result)
{   
    this.temp = result;
    if (this.temp != 0)
    {
        if (this.mode == 0)
        {
            this.temp = this.input1 * 10 + input;
            this.input1 = this.temp;
        }
        else
        {
            this.temp = this.input2 * 10 + input;
            this.input2 = this.temp;
        }

        return this.temp;
    }
    else
    {
        this.input1 = input;
        return input;
    }
}; //end calculator_process.prototype.main_process

calculator_process.prototype.div = function()
{
    this.input1 = this.exec();
    this.mode = 4; //除法
    
    if (this.input2 != 0)
    {
        this.input1 = this.input1 / this.input2;
        this.input2 = 0;
    }

    return this.input1;
}; //end calculator_process.prototype.div

calculator_process.prototype.mul = function()
{
    this.input1 = this.exec();
    this.mode = 3; //乘法
    
    if (this.input2 != 0)
    {
        this.input1 *= this.input2;
        this.input2 = 0;
    }
    
    return this.input1;
}; //end calculator_process.prototype.mul

calculator_process.prototype.sub = function()
{
    this.input1 = this.exec();
    this.mode = 2; //減法
    
    if (this.input2 != 0)
    {
        this.input1 = this.input1 - this.input2;
        this.input2 = 0;
    }

    return this.input1;
}; //end calculator_process.prototype.sub

calculator_process.prototype.back = function(result)
{
    this.temp = result;
    if (this.temp != 0)
    {
        if (this.mode == 0)
        {
            this.temp = (this.input1 - (this.input1 % 10)) / 10;
            this.input1 = this.temp;
        }
        else
        {
            this.temp = (this.input2 - (this.input2 % 10)) / 10;
            this.input2 = this.temp;
        }

        return this.temp;
    }
    else
    {
        return 0;
    }
}; //end calculator_process.prototype.back

calculator_process.prototype.add = function()
{
    this.input1 = this.exec();
    this.mode = 1; //加法
    
    if (this.input2 != 0) {
        this.input1 = this.input1 + this.input2;
        this.input2 = 0;
    }

    return this.input1;
}; //end calculator_process.prototype.add

calculator_process.prototype.equ = function()
{
    this.input1 = this.exec();
    this.mode = 0;

    return this.input1;
}; // end calculator_process.prototype.equ

var vm = new Vue
(
    {
        el: "#calculator",
        data:
        {
            calculator: new calculator_process(),
            result: 0
        },
        mounted: function()
        {
            window.addEventListener("keydown", function(event)
            {
                var res = event.which;
                if (!(
                        ((res >= 48) && (res <= 57)) ||
                        ((res >= 96) && (res <= 105))
                    )) //輸入值不是0~9
                {
                    res = res;
                }
                else if ((res - 96) < 0)
                {
                    res -= 48;
                    this.result = this.calculator.main_process(res, this.result);
                }
                else
                {
                    res -= 96;
                    this.result = this.calculator.main_process(res, this.result);
                }

                switch (res)
                {
                    //add
                    case 107:
                        this.result = this.calculator.add();
                        break;
                    //sub
                    case 109:
                        this.result = this.calculator.sub();
                        break;
                    //mul
                    case 106:
                        this.result = this.calculator.mul();
                        break;
                    //div
                    case 111:
                        this.result = this.calculator.div();
                        break;
                    //enter
                    case 13:
                        this.result = this.calculator.equ();
                        break;
                    //back
                    case 8:
                        this.result = this.calculator.back(this.result);
                        break;
                    default:
                        break;
                } //end switch
            }.bind(this));
        },
        methods:
        {
            main_process: function(input)
            {
                this.result = this.calculator.main_process(input, this.result);
            },
            div: function()
            {
                this.result = this.calculator.div();
            },
            mul: function()
            {
                this.result = this.calculator.mul();
            },
            sub: function()
            {
                this.result = this.calculator.sub();
            },
            back: function()
            {
                this.result = this.calculator.back(this.result);
            },
            add: function()
            {
                this.result = this.calculator.add();
            },
            equ: function()
            {
                this.result = this.calculator.equ();
            }
        }
    }
);