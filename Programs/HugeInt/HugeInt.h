#ifndef HUGEINT_H
#define HUGEINT_H

#if (__cplusplus != 201103L) && (__cplusplus != 201402L) && (__cplusplus != 201703L)
#error only supports c++11, c++14 or c++17
#endif

/*keep it stupid and simple!*/
#include <string>
#include <vector>
#include <cstring>
#include <regex>

class HugeInt
{
    /*friend function*/
    friend istream& operator >> (istream &, HugeInt &);

    friend ostream& operator << (ostream &, HugeInt &);

public:

    HugeInt(long = 0);

    HugeInt(const string &);

    HugeInt(const char *);

    HugeInt operator + (const HugeInt &) const;

    HugeInt operator - (const HugeInt &) const;

    HugeInt operator * (const HugeInt &) const;

    HugeInt operator / (const HugeInt &) const;

    HugeInt operator % (const HugeInt &) const;

    HugeInt abs() const;

    bool operator > (const HugeInt &) const;

    bool operator < (const HugeInt &) const;

    bool operator == (const HugeInt &) const;
    
    bool operator != (const HugeInt &) const;

    void set_integer(const vector<short> &);
    void set_integer(const string &);

    string printInt() const;

private:

    vector<short> integer;

    int count; //計算位數

    void process_string(const string &);

}; //class HugeInt
#endif //HUGEINT_H