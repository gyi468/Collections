#include "HugeInt.h"

using namespace std;

/*friend function*/
istream& operator >> (istream &is, HugeInt &h)
{
    string input;
    is >> input;
    h.set_integer(input);
    return is;
} //end istream& operator >>

ostream& operator << (ostream &os, HugeInt &h)
{
    os << h.printInt();
    return os;
} //end ostream& operator <<

/*constructor*/
HugeInt::HugeInt(long input):
count(0)
{
    int index(40);
    long temp(0);
    short res(0);
    vector<short> temp_vector(41, 0);
    integer = temp_vector;

    //input < 0
    if (input < 0)
    {
        integer.at(0) = 1;
        temp = -input;
    }
    else
    {
        temp = input;
    }

    int sf = 0; //stop flag

    for (index = 40; index >= 1 && sf != 1; --index)
    {
        res = (short)(temp % 10);
        temp = temp - (temp % 10);
        temp /= 10;
        integer.at(index) = res;
        ++count;

        if (temp == 0)
        {
            sf = 1;
            continue;
        }
    }
} //end HugeInt::HugeInt

HugeInt::HugeInt(const string &input):
count(0)
{
    process_string(input);
} //end HugeInt::HugeInt

HugeInt::HugeInt(const char *char_arr):
count(0)
{
    string input = char_arr;
    process_string(input);
} //end HugeInt::HugeInt

/*public menber function*/
HugeInt HugeInt::operator + (const HugeInt &input) const
{
    int index(0);
    vector<short> res(41, 0);

    // 一正一負
    if (integer.at(0) != input.integer.at(0))
    {
        if (integer.at(0) == 0)
        {
            return (abs() - input.abs());
        }
        else
        {
            return (input.abs() - abs());
        }
    }

    //這裡只會有兩個正或兩個負的狀況
    int carry = 0;
    for (index = 40; index >= 1; --index)
    {
        res.at(index) = integer.at(index) + input.integer.at(index) + carry;
        if (res.at(index) >= 10)
        {
            res.at(index) -= 10;
            carry = 1;
        }
        else
        {
            carry = 0;
        }

        //overflow
        if ((index == 1) && (carry == 1))
        {
            throw "Overflow!";
        }
    }

    //補上正負號
    res.at(0) = integer.at(0);

    HugeInt ret;
    ret.set_integer(res);
    return ret;
} //end HugeInt::operator +

HugeInt HugeInt::operator - (const HugeInt &input) const
{
    //減數為負號的狀況
    if (input.integer.at(0) == 1)
    {
        return (*this + input.abs());
    }

    //負減正
    if (integer.at(0) == 1)
    {
        HugeInt ret;

        ret = abs() + input;
        ret.integer.at(0) = 1;

        return ret;
    }

    vector<short> par1; //被減數
    vector<short> par2; //減數
    vector<short> res(41, 0); //結果

    /*
    減法用兩個相減為正的數處理
    減少不必要的錯誤
    */
    //正數減正數
    if (integer.at(0) == 0)
    {
        if (*this > input)
        {
            par1 = integer;
            par2 = input.integer;
        }
        else if (*this < input)
        {
            par1 = input.integer;
            par2 = integer;
            res.at(0) = 1;
        }
        else
        {
            HugeInt ret;
            return ret;
        }
    }

    int index(0);
    int carry = 0;
    for (index = 40; index >= 1; --index)
    {
        res.at(index) = par1.at(index) - par2.at(index) - carry;

        if (res.at(index) < 0)
        {
            res.at(index) += 10; //補位
            carry = 1; //借位
        }
        else
        {
            carry = 0;
        }

        //overflow
        if ((index == 1) && (carry == 1))
        {
            throw "Overflow!";
        }
    } //end for

    HugeInt ret;
    ret.set_integer(res);
    return ret;
} //end HugeInt::operator -

HugeInt HugeInt::operator * (const HugeInt &input) const
{
    //check overflow 最極端的狀況
    if ((count + input.count) > 40)
    {
        throw "Overflow!";
    }

    HugeInt ref; //zero, for reference

                 //乘以0
    if (input == ref)
    {
        return ref;
    }

    vector<short> res(41, 0);

    int index1(0); //乘數起始位置
    int temp1(40 - input.count + 1); //乘數結束位置
    int index2(0); //被乘數起始位置
    int temp2(40 - count + 1); //被乘數結束位置
    int carry = 0;
    int counter = 0;
    HugeInt ret;
    HugeInt process;
    for (index1 = 40; index1 >= temp1; --index1)
    {
        for (index2 = 40; index2 >= temp2; --index2)
        {
            res.at(index2 - counter) = input.integer.at(index1) * integer.at(index2) + carry;

            if (res.at(index2 - counter) > 10)
            {
                carry = res.at(index2 - counter) / 10;
                res.at(index2 - counter) %= 10;
            }
            else
            {
                carry = 0;
            }

            //處理多出來的一位數
            if (index2 == temp2 && carry != 0)
            {
                res.at(index2 - counter - 1) = carry;
            }
        } //end for index2

        process.set_integer(res);
        ret = ret + process;

        //reset
        vector<short> temp(41, 0);
        res = temp;
        carry = 0;

        //add counter
        ++counter;
    }

    //處理正負號
    if (integer.at(0) != input.integer.at(0))
    {
        ret.integer.at(0) = 1;
    }
    else
    {
        ret.integer.at(0) = 0;
    }

    return ret;
} //end HugeInt::multiply

HugeInt HugeInt::operator / (const HugeInt &input) const
{
    HugeInt ret; //return value
    HugeInt ref; //zero, for reference

    //除數為0
    if (input == ref)
    {
        throw "Div Zero!";
    }

    //最極端的狀況
    if (abs() < input.abs())
    {
        return ref;
    }

    ref.integer.at(40) = 1;
    if (input.abs() == ref)
    {
        ret = *this;
        if (integer.at(0) != input.integer.at(0))
        {
            ret.integer.at(0) = 1;
            return ret;
        }
        else
        {
            return ret;
        }
    }

    //reset ref
    ref.integer.at(40) = 0;

    vector<short> res(41, 0);
    vector<short> par1(41, 0);
    HugeInt process1; //用來當被除數
    HugeInt process2; //用來當除法運算時 減被除數的對象
    HugeInt process3; //用來try and error

    //initialize
    process1.set_integer(integer);

    int temp_var(40 - (count - input.count + 1) + 1); //商數最多只有這麼多位數
    int index1(temp_var); //商數起始位置
    int sf(0);

    while (sf != 1 && index1 <= 40)
    {
        /*
        用linear search
        原因是目標沒有明確的值
        */
        int offset1 = index1 - input.count + 1;
        const int offset2 = 40 - input.count + 1;
        std::memcpy(par1.data() + offset1, input.integer.data() + offset2, input.count * sizeof(short));

        process2.set_integer(par1);
        bool carry_flag(process2 > process1);

        //會多借一位的狀況
        if (carry_flag)
        {
            //因為res被初始化成全部為0 所以不處理res
            if (index1 >= 40)
            {
                sf = 1;
                continue; //記憶體會越界 所以continue
            }

            //reset process2 and par1
            vector<short> temp_vec(41, 0);
            process2.set_integer(temp_vec);
            par1 = temp_vec;

            //多借一位
            ++offset1;
            std::memcpy(par1.data() + offset1, input.integer.data() + offset2, input.count * sizeof(short));
            process2.set_integer(par1);
        }

        //linear search
        int stop_flag = 0;
        int index2(1);
        for (index2 = 1; index2 <= 9 && stop_flag != 1; ++index2)
        {
            process3.integer.at(40) = (short)index2;
            if ((process2 * process3) > process1)
            {
                stop_flag = 1;
                index2 -= 2; //因為經過for的時候還會再加1 所以這邊要減2
                continue;
            }
        }

        if (index2 == 10)
        {
            --index2;
        }

        process3.integer.at(40) = (short)index2;
        process1 = process1 - (process2 * process3);

        //處理標記位置
        if (carry_flag)
        {
            res.at(index1 + 1) = (short)index2;
            index1 += 2;
        }
        else
        {
            res.at(index1) = (short)index2;
            ++index1;
        }

        //reset process2 and par1
        vector<short> temp_vec(41, 0);
        process2.set_integer(temp_vec);
        par1 = temp_vec;
    } //end while


    ret.set_integer(res);
    //處理正負號
    if (ret != ref)
    {
        if (integer.at(0) != input.integer.at(0))
        {
            ret.integer.at(0) = 1;
        }
        else
        {
            ret.integer.at(0) = 0;
        }
    }

    return ret;
} //end HugeInt::operator /

HugeInt HugeInt::operator % (const HugeInt &input) const
{
    HugeInt ret; //return value
    HugeInt ref; //zero, for reference

    //除數為0
    if (input == ref)
    {
        throw "Div Zero!";
    }

    //最極端的狀況
    if (abs() < input.abs())
    {
        return *this;
    }

    ref.integer.at(40) = 1;
    if (input.abs() == ref)
    {
        ref.integer.at(40) = 0;
        return ref;
    }

    //reset ref
    ref.integer.at(40) = 0;

    vector<short> res(41, 0);
    vector<short> par1(41, 0);
    HugeInt process1; //用來當被除數
    HugeInt process2; //用來當除法運算時 減被除數的對象
    HugeInt process3; //用來try and error

    //initialize
    process1.set_integer(integer);

    int temp_var(40 - (count - input.count + 1) + 1); //商數最多只有這麼多位數
    int index1(temp_var); //商數起始位置
    int sf(0);

    while (sf != 1 && index1 <= 40)
    {
        /*
        用linear search
        原因是目標沒有明確的值
        */
        int offset1 = index1 - input.count + 1;
        const int offset2 = 40 - input.count + 1;
        std::memcpy(par1.data() + offset1, input.integer.data() + offset2, input.count * sizeof(short));

        process2.set_integer(par1);
        bool carry_flag(process2 > process1);

        //會多借一位的狀況
        if (carry_flag)
        {
            //因為res被初始化成全部為0 所以不處理res
            if (index1 >= 40)
            {
                sf = 1;
                continue; //記憶體會越界 所以continue
            }

            //reset process2 and par1
            vector<short> temp_vec(41, 0);
            process2.set_integer(temp_vec);
            par1 = temp_vec;

            //多借一位
            ++offset1;
            std::memcpy(par1.data() + offset1, input.integer.data() + offset2, input.count * sizeof(short));
            process2.set_integer(par1);
        }

        //linear search
        int stop_flag = 0;
        int index2(1);
        for (index2 = 1; index2 <= 9 && stop_flag != 1; ++index2)
        {
            process3.integer.at(40) = (short)index2;
            if ((process2 * process3) > process1)
            {
                stop_flag = 1;
                index2 -= 2; //因為經過for的時候還會再加1 所以這邊要減2
                continue;
            }
        }

        if (index2 == 10)
        {
            --index2;
        }

        process3.integer.at(40) = (short)index2;
        process1 = process1 - (process2 * process3);

        //處理標記位置
        if (carry_flag)
        {
            res.at(index1 + 1) = (short)index2;
            index1 += 2;
        }
        else
        {
            res.at(index1) = (short)index2;
            ++index1;
        }

        //reset process2 and par1
        vector<short> temp_vec(41, 0);
        process2.set_integer(temp_vec);
        par1 = temp_vec;
    } //end while

    //處理正負號
    if (process1 != ref)
    {
        if (integer.at(0) != input.integer.at(0))
        {
            process1.integer.at(0) = 1;
        }
        else
        {
            process1.integer.at(0) = 0;
        }
    }

    return process1;
} //end HugeInt::operator %

HugeInt HugeInt::abs() const
{
    vector<short> res = integer;
    HugeInt ret;
    res.at(0) = 0;
    ret.set_integer(res);
    return ret;
} //end HugeInt::abs()

bool HugeInt::operator > (const HugeInt &input) const
{
    //比正負號
    if (integer.at(0) != input.integer.at(0))
    {
        if (integer.at(0) == 1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /*兩數正負號相同*/
    //比位數
    if (count != input.count)
    {
        if (integer.at(0) == 1) //兩者皆負
        {
            return (count < input.count); //負的位數越少越大
        }
        else
        {
            return (count > input.count); //正的位數越多越大
        }
    }

    if (*this == input)
    {
        return false;
    }

    //一位一位比較 正數為準
    bool res = true;
    if (count == input.count)
    {
        int sf = 0;
        int index(0);
        for (index = (40 - count + 1); index <= 40 && sf != 1; ++index)
        {
            if (integer.at(index) == input.integer.at(index))
            {
                continue;
            }
            else if (integer.at(index) > input.integer.at(index))
            {
                sf = 1;
                res = true;
                continue;
            }
            else
            {
                sf = 1;
                res = false;
                continue;
            }
        }
    }

    //考慮到負數的狀況
    if (integer.at(0) == 1)
    {
        return (!res);
    }

    return res;
} //end HugeInt::operator >

bool HugeInt::operator < (const HugeInt &input) const
{
    if (*this > input ||
        *this == input)
    {
        return false;
    }
    else
    {
        return true;
    }
}

bool HugeInt::operator == (const HugeInt &input) const
{
    if (integer.at(0) != input.integer.at(0))
    {
        return false;
    }

    /*兩數正負號相同*/
    //比位數
    if (count != input.count)
    {
        return false;
    }

    bool res(true);
    int sf = 0;
    int index(0);
    for (index = (40 - count + 1); index <= 40 && sf != 1; ++index)
    {
        if (integer.at(index) != input.integer.at(index))
        {
            res = false;
            sf = 1;
            continue;
        }
    }
    return res;
} //end HugeInt::operator ==

bool HugeInt::operator != (const HugeInt &input) const
{
    return (!(*this == input));
} //end HugeInt::operator !=

void HugeInt::set_integer(const vector<short> &input)
{
    integer = input;
    int index(0);
    int sf(0);
    for (index = 1; index <= 40 && sf != 1; ++index)
    {
        if (input.at(index) == 0)
        {
            continue;
        }
        else
        {
            sf = 1;
            count = 40 - index + 1;
            continue;
        }
    }
} //end HugeInt::set_integer

void HugeInt::set_integer(const string &input)
{
    process_string(input);
} //end HugeInt::set_integer

string HugeInt::printInt() const
{
    string ret("");
    int index(40 - count + 1);
    if (integer[0] == 1)
    {
        ret += "-";
    }

    for (index = (40 - count + 1); index <= 40; ++index)
    {
        ret += to_string(integer.at(index));
    }

    return ret;
}

/*private member function*/
void HugeInt::process_string(const string &input)
{
    counter = 0; //reset counter
    vector<short> temp_vector(41, 0);
    integer = temp_vector;

    //check input
    regex number("^-?[0-9]{1,42}$");
    if (!regex_match(input, number))
    {
        throw "Input is not number!";
    }

    int length = (int)input.size();
    if ((length > 41) ||
        ((length == 41) && (input.at(0) != '-')))
    {
        throw "Number is out of range";
    }

    --length; //定位到input的最後一個元素

    //檢查輸入是否為負數
    bool negative = false;
    if (input.at(0) == '-')
    {
        integer.at(0) = 1;
        negative = true;
    }

    int index(40); //最後一個元素
    int sf = 0;
    for (index = 40; index >= 1 && sf != 1; --index)
    {
        integer.at(index) = (short)(input.at(length) - '0');
        --length;
        ++count;

        if (negative)
        {
            if (length == 0)
            {
                sf = 1;
                continue;
            }
        }
        else
        {
            if (length < 0)
            {
                sf = 1;
                continue;
            }
        }
    } //end for
}