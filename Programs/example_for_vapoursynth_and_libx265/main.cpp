﻿/*
 * This program is just for reference and example. By gyi468.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111, USA.
 */

#include <iostream>
#include <ctime>
#include "x265.h"
#include "vapoursynth/VSScript.h"
#include "vapoursynth/VSHelper.h"

#include "Windows.h"

using namespace std;

bool parseQPFile(x265_param *param, x265_picture *pic, FILE *qpfile)
{
	int32_t num = -1, qp, ret;
	char type;
	long filePos;
	pic->forceqp = 0;
	pic->sliceType = X265_TYPE_AUTO;
	while (num < pic->poc)
	{
		filePos = ftell(qpfile);
		qp = -1;
		ret = fscanf_s(qpfile, "%d %c%*[ \t]%d\n", &num, &type, &qp);

		if (num > pic->poc || ret == EOF)
		{
			fseek(qpfile, filePos, SEEK_SET);
			break;
		}
		if (num < pic->poc && ret >= 2)
			continue;
		if (ret == 3 && qp >= 0)
			pic->forceqp = qp + 1;
		if (type == 'I') pic->sliceType = X265_TYPE_IDR;
		else if (type == 'i') pic->sliceType = X265_TYPE_I;
		else if (type == 'K') pic->sliceType = param->bOpenGOP ? X265_TYPE_I : X265_TYPE_IDR;
		else if (type == 'P') pic->sliceType = X265_TYPE_P;
		else if (type == 'B') pic->sliceType = X265_TYPE_BREF;
		else if (type == 'b') pic->sliceType = X265_TYPE_B;
		else ret = 0;
		if (ret < 2 || qp < -1 || qp > 51)
			return 0;
	}
	return 1;
}

void report_stats(const x265_api *api, x265_encoder *encoder,
				  x265_stats *stats, const VSVideoInfo *vi)
{
	api->encoder_get_stats(encoder, stats, sizeof(x265_stats));
	double x265_fps = stats->encodedPictureCount / (stats->elapsedEncodeTime);
	double progress = (stats->encodedPictureCount / (vi->numFrames)) * 100;

	if (x265_fps > 0.)
	{
		uint32_t eta = static_cast<uint32_t>(vi->numFrames - stats->encodedPictureCount / x265_fps);
		printf("[%.1f\%] fps: %.2f eta: %d:%d:%d\r", progress, x265_fps,
			   eta / 3600, (eta / 60) % 60, eta % 60);
	}
	else
	{
		printf("[%.1f\%] fps: %.2f eta: unknown\r", progress, x265_fps);
	}

	fflush(stdout);
}

int main(int argc, char *argv[])
{
	if (argc != 3) {
		cerr << "Usage: example <infile> <qpfile> <outfile>" << endl;
		return 1;
	}

	if (SetPriorityClass(GetCurrentProcess(), IDLE_PRIORITY_CLASS) == 0)
	{
		cerr << "Fail to set this program's priority!" << endl;
		return 1;
	}

	//Vapoursynth init
	if (vsscript_init() == 0)
	{
		cerr << "Failed to initialize VapourSynth environment!" << endl;
		return 1;
	}

	VSScript *se = nullptr;
	if (vsscript_evaluateFile(&se, argv[1], efSetWorkingDir))
	{
		cerr << "Script evaluation failed." << endl;
		vsscript_freeScript(se);
		vsscript_finalize();
		return 1;
	}

	const VSAPI *vsapi = nullptr;
	vsapi = vsscript_getVSApi();
	if (!vsapi)
	{
		cerr << "Failed to retrieve vsapi!" << endl;

		vsscript_freeScript(se);
		vsscript_finalize();
		return 1;
	}

	VSNodeRef *node = nullptr;
	node = vsscript_getOutput(se, 0);
	if (!node)
	{
		cerr << "Failed to retrieve output node!" << endl;

		vsscript_freeScript(se);
		vsscript_finalize();
		return 1;
	}

	const VSVideoInfo *vi = nullptr;
	vi = vsapi->getVideoInfo(node);
	if (!isConstantFormat(vi) || !vi->numFrames)
	{
		cerr << "Cannot output clips with varying dimensions or unknown length!" << endl;
		vsapi->freeNode(node);
		vsscript_freeScript(se);
		vsscript_finalize();
		return 1;
	}

	if (vi->format->colorFamily != cmYUV ||
		vi->format->sampleType != stInteger)
	{
		cerr << "Script only support YUV and integer format." << endl;;

		vsapi->freeNode(node);
		vsscript_freeScript(se);
		vsscript_finalize();
		return 1;
	}

	//only accept 8 10 12 input
	if (vi->format->bitsPerSample != 8 &&
		vi->format->bitsPerSample != 10 &&
		vi->format->bitsPerSample != 12)
	{
		cerr << "Script only support 8 or 10 or 12bits!" << endl;

		vsapi->freeNode(node);
		vsscript_freeScript(se);
		vsscript_finalize();
		return 1;
	}

	//find csp
	int temp_csp = vi->format->id;
	int csp = 0;
	switch (temp_csp)
	{
	case pfYUV420P8:
	case pfYUV420P10:
	case pfYUV420P12:
		csp = 420;
		break;
	case pfYUV422P8:
	case pfYUV422P10:
	case pfYUV422P12:
		csp = 422;
		break;
	case pfYUV444P8:
	case pfYUV444P10:
	case pfYUV444P12:
		csp = 444;
		break;
	default:
		cerr << "Script has unsupport csp!" << endl;

		vsapi->freeNode(node);
		vsscript_freeScript(se);
		vsscript_finalize();
		return 1;
	}

	//libx265 init
	const x265_api *api = nullptr;
	api = x265_api_get(vi->format->bitsPerSample);
	if (!api)
	{
		cerr << "Fail to get x265 api!" << endl;
		vsapi->freeNode(node);
		vsscript_freeScript(se);
		vsscript_finalize();
		return 1;
	}

	x265_param *param = nullptr;
	param = api->param_alloc();
	if (!param)
	{
		cerr << "Fail to get x265 param!" << endl;
		api->cleanup();
		vsapi->freeNode(node);
		vsscript_freeScript(se);
		vsscript_finalize();
		return 1;
	}

	//x265 picture
	x265_picture *pic = nullptr;
	pic = api->picture_alloc();
	if (!pic)
	{
		cerr << "Fail to get x265 pic!" << endl;
		api->param_free(param);
		api->cleanup();
		vsapi->freeNode(node);
		vsscript_freeScript(se);
		vsscript_finalize();
		return 1;
	}

	if (api->param_default_preset(param, "medium", nullptr) != 0)
	{
		cerr << "x265 warning: Fail to set preset." << endl;
		api->picture_free(pic);
		api->param_free(param);
		api->cleanup();
		vsapi->freeNode(node);
		vsscript_freeScript(se);
		vsscript_finalize();
		return 1;
	}

	param->sourceWidth = vi->width;
	param->sourceHeight = vi->height;

	//set csp
	switch (csp)
	{
	case 420:
		param->internalCsp = X265_CSP_I420;
		break;
	case 422:
		param->internalCsp = X265_CSP_I422;
		break;
	case 444:
		param->internalCsp = X265_CSP_I444;
		break;
	}

	param->fpsNum = static_cast<uint32_t>(vi->fpsNum);
	param->fpsDenom = static_cast<uint32_t>(vi->fpsDen);

	int ret = api->param_parse(param, "crf", "23");
	if (ret != 0)
	{
		if (ret == -1)
		{
			cerr << "x265 bad name : crf" << endl;
		}
		else
		{
			cerr << "x265 bad value: 23" << endl;
		}

		api->picture_free(pic);
		api->param_free(param);
		api->cleanup();
		vsapi->freeNode(node);
		vsscript_freeScript(se);
		vsscript_finalize();

		return 1;
	}

	// Open the output file for writing
	FILE *dst, *qpfile;
	if (fopen_s(&dst, argv[3], "wb") != 0 || fopen_s(&qpfile, argv[2], "rb") != 0)
	{
		cerr << "Failed to open output or qpfile." << endl;
		api->picture_free(pic);
		api->param_free(param);
		api->cleanup();
		vsapi->freeNode(node);
		vsscript_freeScript(se);
		vsscript_finalize();
		return 1;
	}

	x265_encoder *encoder = nullptr;
	encoder = api->encoder_open(param);
	if (!encoder)
	{
		cerr << "Fail to open x265 encoder!" << endl;
		api->picture_free(pic);
		api->param_free(param);
		api->cleanup();
		vsapi->freeNode(node);
		vsscript_freeScript(se);
		vsscript_finalize();
		fclose(dst);
		fclose(qpfile);
		return 1;
	}

	//main loop encode
	x265_nal *pNals = nullptr;
	uint32_t iNal = 0;

	//write header
	if (!param->bRepeatHeaders)
	{
		if (api->encoder_headers(encoder, &pNals, &iNal) < 0)
		{
			cerr << "x265 error: Failure generating stream headers" << endl;
			api->encoder_close(encoder);
			api->picture_free(pic);
			api->param_free(param);
			api->cleanup();
			vsapi->freeNode(node);
			vsscript_freeScript(se);
			vsscript_finalize();
			fclose(dst);
			fclose(qpfile);
			return 1;
		}
		else
		{
			for (uint32_t j = 0; j < iNal; ++j)
			{
				fwrite(pNals[j].payload, 1, pNals[j].sizeBytes, dst);
			}
		}
	}

	int fail_flag(0);
	api->picture_init(param, pic);
	x265_stats *stats = new x265_stats;
	time_t time1, time2;
	time1 = time(nullptr);
	for (int i = 0; i < vi->numFrames; ++i)
	{
		char errMsg[1024];
		const VSFrameRef *frame = vsapi->getFrame(i, node,
			errMsg, sizeof(errMsg));
		if (!frame)
		{
			cerr << errMsg << endl;
			fail_flag = 1;
			break;
		}

		pic->planes[0] = (void *)(vsapi->getReadPtr(frame, 0));
		pic->planes[1] = (void *)(vsapi->getReadPtr(frame, 1));
		pic->planes[2] = (void *)(vsapi->getReadPtr(frame, 2));

		pic->stride[0] = vsapi->getStride(frame, 0);
		pic->stride[1] = vsapi->getStride(frame, 1);
		pic->stride[2] = vsapi->getStride(frame, 2);

		pic->poc = i;
		if (qpfile != nullptr)
		{
			if (!parseQPFile(param, pic, qpfile))
			{
				fprintf(stderr, "x265 error: Can't parse qpfile for frame %d.", i);
				fail_flag = 1;
				break;
			}
		}

		api->encoder_encode(encoder, &pNals, &iNal, pic, nullptr);
		for (uint32_t j = 0; j < iNal; ++j)
		{
			fwrite(pNals[j].payload, 1, pNals[j].sizeBytes, dst);
		}

		time2 = time(nullptr);
		if (difftime(time2, time1) > 2)
		{
			report_stats(api, encoder, stats, vi);
			time1 = time(nullptr); //重新計時
		}

	}  //end main encode loop

	//flush encoder
	ret = 0;
	while (fail_flag == 0 && true)
	{
		ret = api->encoder_encode(encoder, &pNals, &iNal, nullptr, nullptr);
		if (ret == 0)
		{
			break;
		}

		for (uint32_t j = 0; j < iNal; ++j)
		{
			fwrite(pNals[j].payload, 1, pNals[j].sizeBytes, dst);
		}
	}

	report_stats(api, encoder, stats, vi);

	delete stats;
	api->encoder_close(encoder);
	api->picture_free(pic);
	api->param_free(param);
	api->cleanup();
	vsapi->freeNode(node);
	vsscript_freeScript(se);
	vsscript_finalize();
	fclose(dst);
	fclose(qpfile);

	return fail_flag;
}