# Example for VapourSynth and x265

## Introduction

This program in just for example and reference.

## Dependencies

- [VapourSynth](https://github.com/vapoursynth/vapoursynth)
- [x265](http://x265.org)

## Licence

[GNU GPLv2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)