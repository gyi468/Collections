---
title: FreeBSD的SSH Tunnel
date: 2018-03-17 19:41:01
tags:
- FreeBSD
categories:
- BSD Unix
---
只要在伺服器有服務監聽端口  
不管防火牆是否有開放  
透過SSH隧道就能直接連上遠端的端口  
在Linux中 到這裡SSH隧道就大功告成  
但是在FreeBSD 這樣還是無法連線  
<!-- more -->

解決方法:  
在遠端伺服器的防火牆(例如: PF)的規則中  
加入允許遠端伺服器自身的IP通過防火牆  
這樣SSH隧道才能完成它的工作