---
title: Arch Linux桌面環境安裝的簡易筆記
date: 2017-06-10 00:27:57
tags:
- archlinux
- x11
- wayland
- 桌面環境
categories:
- linux
---
# 目錄

- [基本材料](#基本材料)
- [安裝桌面環境](#安裝桌面環境)
- [Post-installation](#post-installation)
- [Reference](#reference)

# 基本材料

- X11
  - 可以安裝的桌面環境: 全部
  - 安裝X-Server需要的程式
    - xorg-server
    - xorg-xinit
    - mesa
  - 測試X-Server需要的程式
    - xorg-twm
    - xterm
    - xorg-xclock
<!-- more -->

- Wayland
  - 可以安裝的桌面且我測試過的: GNOME3 KDE
  - 測試Wayland所需的程式
    - weston
  - 安裝完畢後，請在自己的帳戶的家目錄下的.bashrc設定環境變數XDG_RUNTIME_DIR
    - 在.bashrc中加入以下內容
      ```
      export XDG_RUNTIME_DIR=/path/to/your/folder
      ```
    - 其中/path/to/your/folder請把它改成您要的目錄
    - 該目錄(/path/to/your/folder)的擁有者必須是您 且存取權限必須是700

- 您的電腦的顯示卡的驅動程式
- 如果您用筆記型電腦，請再加裝touch pad的驅動程式
- Pulse Audio

## 測試X11或Wayland
把基本材料安裝完畢後，請重新開機再測試
- X11
  ```bash
  sudo startx
  ```

- Wayland
  - 執行weston
    ```bash
    weston
    ```
  - 測試完確定可以正常執行後，請切換到別的tty，並停止weston
    ```bash
    sudo killall weston
    ```

# 安裝桌面環境

## Wayland

- 安裝gnome或KDE
- 安裝xorg-server-xwayland
- 開啟並啟動gdm(gnome)或sddm(KDE)

## X11

- 安裝您自己喜歡的桌面環境，例如:
  - XFCE
  - KDE
  - LXDE
  - GNOME
  - Cinnamon
- 安裝您喜歡的display manager，例如:
  - LightDM
  - GDM
  - SDDM
  - SLiM
- 開啟並啟動您安裝的display manager

# Post-installation

- 安裝network-manager-applet及networkmanager
- 關閉並停止dhcpcd
- 開啟並啟動NetworkManager
- 到這邊桌面環境的基本安裝就完成了

# Reference

- [ArchWiki - Xorg](https://wiki.archlinux.org/index.php/Xorg)
- [ArchWiki - Wayland](https://wiki.archlinux.org/index.php/Wayland)
- [Installing GUI (Cinnamon Desktop) and Basic Softwares in Arch Linux](https://www.tecmint.com/install-cinnamon-desktop-in-arch-linux/)
- [ArchWiki - GNOME](https://wiki.archlinux.org/index.php/GNOME)