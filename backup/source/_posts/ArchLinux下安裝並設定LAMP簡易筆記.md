---
title: ArchLinux下安裝並設定LAMP簡易筆記
date: 2017-05-26 14:39:13
tags:
- archlinux
- httpd
- mariadb
- php
categories:
- linux
---
# 流程

- 安裝AMP
  ```bash
  pacman -S --need apache mariadb php php-apache
  ```
- 設定apache
  - 檢查apache設定擋(/etc/httpd/conf/httpd.conf)，如果以下這行沒被註解掉，請把它註解掉
    ```
    #LoadModule unique_id_module modules/mod_unique_id.so
    ```
  - 啟用並啟動httpd

- 設定mariadb
  - 初始化資料庫
   ```bash
   mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
   ```
  - 啟用並啟動mysqld
  - 執行mysql_secure_installation
<!-- more -->

- 設定php
  - 編輯apache設定擋，確認以下的內容和設定擋一致
  - 沒被註解的註解掉，被註解的請把註解拿掉，沒有的內容請自己補上去
    ```
    #LoadModule mpm_event_module modules/mod_mpm_event.so
    LoadModule mpm_prefork_module modules/mod_mpm_prefork.so
    LoadModule php7_module modules/libphp7.so
    AddHandler php7-script php
    Include conf/extra/php7_module.conf
    ```
  - 重新啟動httpd

以上AMP就設定完成了  
實際操作過沒問題

主要資料來源: [Install Apache, MariaDB, PHP (LAMP) stack on Arch Linux 2016](https://www.ostechnix.com/install-apache-mariadb-php-lamp-stack-on-arch-linux-2016/)

## Reference
- [ArchWiki - Apache HTTP Server](https://wiki.archlinux.org/index.php/Apache_HTTP_Server)
- [ArchWiki - MySQL](https://wiki.archlinux.org/index.php/MySQL)