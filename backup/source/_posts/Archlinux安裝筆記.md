---
title: Archlinux安裝筆記
date: 2017-05-14 02:51:54
tags:
- archlinux
categories:
- linux
---
由於[Archlinux](https://www.archlinux.org/)安裝方式特殊  
沒有像其他作業系統一樣  
在安裝程式一步一步引導你安裝及設定  
得去[ArchWiki](https://wiki.archlinux.org/)看說明一步一步來  
我把安裝過程整理消化  
並且把我看不懂的地方整理起來  
實際去操作(Archlinux 2017/05/01的LiveCD安裝)過沒問題  
變成這份筆記  
希望之後如果還要再安裝ArchLinux  
不要再那麼累了

往下閱讀前  
我建議需要一些Linux或BSD的使用經驗和常識  
如果您是Linux新手  
我建議使用[Linux Mint](https://www.linuxmint.com/)或[Ubuntu](https://www.ubuntu.com/)
<!-- more -->

如果您要安裝Windows + Arch Linux雙系統  
且Arch Linux和Windows的系統檔案放在不同硬碟  
並使用UEFI模式開機的話  
要先裝哪個系統都沒關係  

以下內容  
是假設您已經會在cli下操作Linux編寫的  

# 目錄

- [Basic Installation](#basic-installation)
- [Optional Settings](#optional-settings)
- [參考資料](#參考資料)

# Basic Installation

開機進入Live CD之後 開始安裝  

- 檢查網路是否是通的
  ```bash
  ping archlinux.org
  ```

- 確認開機模式  
  ```bash
  ls /sys/firmware/efi/efivars
  ```
  - 如果以上的資料夾有東西，代表您的系統是使用UEFI模式開機，接下來磁區分割時，需要多分割一個EFI磁區

- 磁區分割  
  - 使用fdisk或gdisk分割磁區
  - 如果您磁區分割使用GPT，且需要GRUB2開機的話，需要多一個BIOS開機磁區，大小為1M，不必格式化，但是它的位置必須在該硬碟的前2TB

- 格式化磁區  
  - 其中EFI磁區的格式化方式為  
    ```bash
    mkfs.fat -F32 /dev/sdxY
    ```

- 啟動swap area  
  ```bash
  swapon /dev/sdxY
  ```

- 掛載硬碟
  - 將您的主系統碟掛載在/mnt；如果您有efi磁區，將您的efi磁區掛載在/mnt/boot

- 將檔案下載至Live CD的/mnt (即新系統的 /)  
  ```bash
  pacstrap /mnt base
  ```

- 建立系統的fstab 使系統在開機時能正確掛載磁區  
  ```bash
  genfstab -p -U /mnt > /mnt/etc/fstab
  ```

- chroot到新系統 對新系統進行設定  
  ```bash
  arch-chroot /mnt
  ```

- 連結時區設定到/etc/localtime  
  ```bash
  ln -s /usr/share/zoneinfo/Asia/Taipei /etc/localtime
  ```

- 修改locale.gen與建立locale.conf這兩個檔案來設定系統語言  
  - 用你喜歡的文字編輯器修改/etc/locale.gen的內容，把你想要的語言前面的#拿掉，例如:
    - 拿掉/etc/locale.gen裡面的這三行前面的#，使程式能正確處理簡體中文、繁體中文及英文
      - #en_US.UTF-8 UTF-8
      - #zh_CN.UTF-8 UTF-8
      - #zh_TW.UTF-8 UTF-8

  - 修改完成後 執行以下指令來產生locale檔案  
    ```bash
    locale-gen
    ```

  - 接著設定系統語言  
    - 編輯/etc/locale.conf，在裡面打入LANG="en_US.UTF-8"，這樣系統預設語言就是英文  
      ```bash
      echo "LANG=\"en_US.UTF-8\"" > /etc/locale.conf
      ```

- 設定電腦名稱  
  ```bash
  echo "myhostname" > /etc/hostname
  ```
  請將"myhostname"用你喜歡的名字替代 不要照打以上指令  

- 安裝並設定開機程式  
  ```bash
  pacman -S grub os-prober
  ```
  - 如果您是使用UEFI模式開機 則還需要efibootmgr  
    ```bash
    pacman -S efibootmgr
    ```
  - 如果您的cpu是intel 還要再加裝intel-ucode  
    ```bash
    pacman -S intel-ucode
    ```

  - 安裝grub
    - 如果您的系統是bios開機，用以下指令安裝grub，x對應到您的硬碟編號  
      ```bash
      grub-install /dev/sdx
      ```
    - 如果您的系統是UEFI開機 用以下指令安裝grub，esp_mount對應到您efi磁區掛載的位置  
      ```bash
        grub-install --target=x86_64-efi --efi-directory=esp_mount --bootloader-id=grub
      ```
    - 接著建立grub設定檔，之後從pacman更新Linux Kernel 都得執行以下指令 確保系統能正確開機  
      ```bash
      grub-mkconfig -o /boot/grub/grub.cfg
      ```
    - 複製grub的語言檔案  
      ```bash
      cp /usr/share/locale/en@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo
      ```

- 設定root密碼

- 退出chroot

- 卸載磁區並重新開機

- 重新登入系統後，找出您使用的網卡的名稱，並開啟dhcp

- 設定時間  
  - 如果您要安裝Arch Linux+Windows雙系統，請執行以下指令，讓linux把硬體時間理解為本地時間  
    ```bash
    timedatectl set-local-rtc true
    ```

- 到這邊為止 Arch Linux的基本安裝就完成了

# Optional Settings

## ssh保護

建議挑幾個加上去  
提升系統安全姓  
- ssh禁止root登入，需要root權限請用sudo(如果您從iso安裝FreeBSD，這個是預設值；Linux才需要這設定)

- ssh的port不要監聽在22，改成別的

- ssh禁止使用密碼登入，改成使用公鑰

- ssh用[fail2ban](https://www.fail2ban.org/)保護，backend要改成systemd(fail2ban預設是auto)

- ssh啟用兩步驟驗證

## 設定sudo

- 從pacman安裝sudo
- 用您喜歡的文字編輯器或visudo，以root的身分把/etc/sudoers裡面的這行前面的註解拿掉  
  ```
  %wheel ALL=(ALL) ALL
  ```
- 建立使用者，該使用者必須加入wheel這個群組，例如:  
  ```bash
  useradd -mg users -G wheel -s /bin/bash your_new_user
  ```
  請將your_new_user換成你想要的名字

## 設定alias

在bash shell下，可以在/etc/profile中，加入一些alias，重新登入後生效  
  ```
  alias ls="ls --color"
  alias rm="rm -i"
  alias grep="grep --color=auto"
  ```

# 參考資料

- [Arch Linux：安裝系統](http://www.wlintmp.net/2014/02/arch-linux.html)
- [ArchWiki - Installation guide](https://wiki.archlinux.org/index.php/Installation_guide)
- [ArchWiki - EFI System Partition](https://wiki.archlinux.org/index.php/EFI_System_Partition)
- [ArchWiki - GRUB](https://wiki.archlinux.org/index.php/GRUB)
- [Arch Linux Installation and Configuration Guide with Screenshots](http://www.tecmint.com/arch-linux-installation-guide/)
- [ArchWiki - Users and groups](https://wiki.archlinux.org/index.php/users_and_groups)